from selenium import webdriver
from time import sleep
from credentials import username, password


class Unfollowers:
    def __init__(self, username, password):
        self.driver = webdriver.Firefox()
        self.driver.get("https://www.instagram.com")
        sleep(3)

        fucking_cookies_accept = self.driver.find_element_by_xpath("/html/body/div[2]/div/div/div/div[2]/button[1]")
        fucking_cookies_accept.click();
        
        username_field = self.driver.find_element_by_xpath("/html/body/div[1]/section/main/article/div[2]/div[1]/div/form/div/div[1]/div/label/input")
        username_field.send_keys(username)

        password_field = self.driver.find_element_by_xpath("/html/body/div[1]/section/main/article/div[2]/div[1]/div/form/div/div[2]/div/label/input")
        password_field.send_keys(password)

        login_button = self.driver.find_element_by_xpath('/html/body/div[1]/section/main/article/div[2]/div[1]/div/form/div/div[3]/button')
        login_button.click()
        sleep(7)

        #two factor auth
        code = input('Enter security code:')
        code_type = self.driver.find_element_by_xpath("/html/body/div[1]/section/main/div/div/div[1]/div/form/div[1]/div/label/input")
        code_type.send_keys(code)

        confirm = self.driver.find_element_by_xpath("/html/body/div[1]/section/main/div/div/div[1]/div/form/div[2]/button")
        confirm.click()
        sleep(3)
        
        # ad = self.driver.find_element_by_xpath("/html/body/div[1]/section/main/div/div/div/div[1]/button")
        # ad.click()
        # sleep(3)


    def get_unfollowers(self):
        sleep(3)
        usernames = self.driver.find_element_by_xpath("/html/body/div[1]/section/nav/div[2]/div/div/div[3]/div/div[5]/span/img")
        usernames.click()
        sleep(2)
        self.driver.find_element_by_xpath("/html/body/div[1]/section/nav/div[2]/div/div/div[3]/div/div[5]/div[2]/div[2]/div[2]/a[1]/div/div[2]/div/div/div/div").click()
        sleep(3)
        Following = self.driver.find_element_by_xpath("//a[contains(@href,'/following')]")
        Following.click()
        sleep(3)
        following = self.get_users()
        sleep(3)
        Followers = self.driver.find_element_by_xpath("//a[contains(@href,'/followers')]")
        Followers.click()
        sleep(3)
        followers = self.get_users()
        not_following_back = [user for user in following if user not in followers]
        print(not_following_back)

    def get_users(self):
        sleep(5)
        scroll_box = self.driver.find_element_by_xpath("/html/body/div[5]/div/div/div[2]")
        prev_height, height = 0, 1
        while prev_height != height:
            prev_height = height
            sleep(3)
            height = self.driver.execute_script("""
                arguments[0].scrollTo(0, arguments[0].scrollHeight); 
                return arguments[0].scrollHeight;
                """, scroll_box)
        links = scroll_box.find_elements_by_tag_name('a')
        names = [name.text for name in links if name.text != '']
        close = self.driver.find_element_by_xpath("/html/body/div[5]/div/div/div[1]/div/div[2]/button")
        close.click()
        return names


unfollowers = Unfollowers(username, password)
unfollowers.get_unfollowers()
try:
    unfollowers.driver.close()
except:
    print("Fail")
    unfollowers.driver.close()